export const logger = (req, res, next) => {
  const time = new Date();
  const method = req.method;
  const url = req.url;
  console.log(
    `======Request Log=====\nTime: ${time.toLocaleTimeString()}\nRequest Method: ${method}\nEndpoint: ${url}\n======================`
  );
  next();
};
