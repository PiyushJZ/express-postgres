import { query } from '../db/index.js';
import { insertUserInDepartment } from '../services/department.js';

export const createDepartmentTable = async (req, res) => {
  if (process.env.NODE_ENV !== 'DEV') return res.status(401).end();

  try {
    const text =
      'CREATE TABLE department (user_id INT, department VARCHAR(30), UNIQUE(user_id, department))';
    await query(text);
    return res.status(201).send('Table created');
  } catch (err) {
    res.status(400).end();
  }
};

export const addUserToDepartment = async (req, res, next) => {
  const userId = req.params.userId;
  const department = req.body.department;
  const result = await insertUserInDepartment(userId, department);
  result.message ? res.status(400) : res.status(201);
  res.response = result;
  next();
};

export const getDepartmentCounts = async (req, res, next) => {
  try {
    const text =
      'SELECT department, COUNT(department) FROM department GROUP BY (user_id)';
    const departments = (await query(text)).rows;
    res.response = { departments };
    next();
  } catch (err) {
    res.status(400);
    res.response = { message: 'Could not get departments' };
    next();
  }
};
