import { query } from '../db/index.js';

export const createAccountTable = async (req, res) => {
  if (process.env.NODE_ENV !== 'DEV') return res.status(401).end();

  try {
    await query(
      "CREATE TABLE accounts (user_id INT references users(id), balance real, currency CHAR(3) DEFAULT 'EUR')"
    );
    return res.status(201).end();
  } catch (err) {
    console.log(err);
    res.status(400).end();
  }
};

export const dropAccountTable = async (req, res) => {
  if (process.env.NODE_ENV !== 'DEV') return res.status(401).end();

  try {
    await query('DROP TABLE accounts');
    return res.status(200).end();
  } catch (err) {
    res.status(400).end();
  }
};
