import fs from 'fs';

import { query, db } from '../db/index.js';
import {
  createUser,
  fetchUser,
  deleteUserRecord,
  fetchAllUsers,
  fetchUserProfile,
} from '../services/user.js';

export const createUserTable = async (req, res) => {
  if (process.env.NODE_ENV !== 'DEV') return res.status(401).end();

  try {
    await query(
      'CREATE TABLE users (id SERIAL primary key, firstname VARCHAR(255), lastname VARCHAR(255), gender BOOLEAN, ctime TIMESTAMP, address JSONB)'
    );
    return res.status(201).end();
  } catch (err) {
    res.status(400).end();
  }
};

export const dropUserTable = async (req, res) => {
  if (process.env.NODE_ENV !== 'DEV') return res.status(401).end();

  try {
    await query('DROP TABLE users');
    return res.status(200).end();
  } catch (err) {
    res.status(400).end();
  }
};

export const addUser = async (req, res, next) => {
  const { firstName, lastName, gender, address } = req.body.user;
  // if (firstName && lastName && typeof gender === 'boolean') {
  const result = await createUser(firstName, lastName, gender, address);
  result.message ? res.status(400) : res.status(201);
  res.response = result;
  next();
  return;
  // }
  res.status(400);
  next();
};

export const getUser = async (req, res, next) => {
  const userId = req.params.userId;
  const result = await fetchUser(userId);
  result.message ? res.status(400) : res.status(201);
  res.response = result;
  next();
};

export const deleteUser = async (req, res, next) => {
  const userId = req.params.userId;
  const result = await deleteUserRecord(userId);
  result.message ? res.status(400) : res.status(200);
  res.response = result;
  next();
};

export const getAllUsers = async (req, res, next) => {
  const result = await fetchAllUsers();
  res.response = { users: result };
  next();
};

export const getUserProfile = async (req, res, next) => {
  const userId = req.params.userId;
  const result = await fetchUserProfile(userId);
  res.response = result;
  result.message ? res.status(400) : res.status(200);
  next();
};

export const addUserAddress = async (req, res, next) => {
  const userId = req.params.userId;
  const address = req.body.address;
  const result = await insertUserAddress(userId, address);
};

export const uploadTextFile = async (req, res, next) => {
  const fileBinary = req.file;
  if (fileBinary.originalname.split('.')[1] !== 'txt') {
    res.response = { message: 'Only text files allowed!!' };
    res.status(400);
    next();
    return;
  }
  fs.readFile(fileBinary.path, 'utf8', (err, data) => {
    if (err) {
      console.log(err);
      return;
    }
    const res = data
      .split('\n')
      .filter((x, idx) => {
        return idx % 2 !== 0;
      })
      .join('\n');
    console.log(res);
  });
  next();
};

export const downloadTextFile = async (req, res) => {
  const fileName = req.query.fileName;
  res.status(200).download(`src/temp/${fileName}.txt`, `${fileName}.txt`);
};

export const fetchUsersFromMongo = async (req, res, next) => {
  const users = await db.collection('users').find().toArray();
  console.log(users);
  next();
};
