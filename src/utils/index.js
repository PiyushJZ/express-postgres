import EventEmitter from 'events';

import { getUserEntries } from '../services/user.js';

const eventEmitter = new EventEmitter();

eventEmitter.on('count', entries => {
  console.log(`Users table Entries: ${entries.count}`);
});

export const responseHandler = async (req, res) => {
  res.set({
    Accept: 'application/json',
    'Content-Type': 'application/json',
  });
  res.json(res.response ?? { message: 'No Content' });
};

export const countUserEntriesEvent = async () => {
  const entries = await getUserEntries();
  eventEmitter.emit('count', entries);
};
