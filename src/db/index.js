import pg from 'pg';
import { MongoClient } from 'mongodb';

import config from '../config/index.js';

const { DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME } = config;

const pgConfig = {
  user: DB_USER,
  database: DB_NAME,
  password: DB_PASS || 'enovate',
  host: DB_HOST,
  port: DB_PORT,
};

const pool = new pg.Pool(pgConfig);

export const query = (text, params) => pool.query(text, params);

export const client = async () => {
  const client = await pool.connect();
  return client;
};

const { MONGO_URL } = config;

const mongoClient = new MongoClient(MONGO_URL);
await mongoClient.connect();
export const db = mongoClient.db(DB_NAME);
