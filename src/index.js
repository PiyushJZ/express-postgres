import express from 'express';

import routes from './routes/index.js';
import { logger } from './middlewares/index.js';
import config from './config/index.js';

const { PORT } = config;

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger);

// Routes
app.use('/', routes);

app.listen(PORT, async () => {
  console.log(`Server running on port: ${PORT}`);
});
