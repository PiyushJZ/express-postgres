import express from 'express';
import multer from 'multer';

import {
  createUserTable,
  dropUserTable,
  addUser,
  getUser,
  deleteUser,
  getAllUsers,
  getUserProfile,
  uploadTextFile,
  downloadTextFile,
  fetchUsersFromMongo,
} from '../controllers/userController.js';
import { responseHandler } from '../utils/index.js';

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './src/temp/');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });
// Endpoint to create Users table
router.post('/table', createUserTable);

// Endpoint to drop the Users table
router.delete('/table', dropUserTable);

// Endpoint to insert a new user into the Users table
router.put('/', addUser, responseHandler);

router.get('/mongo', fetchUsersFromMongo, responseHandler);

// Find user and account by user id
router.get('/:userId', getUser, responseHandler);

// Delete a user and account by user id
router.delete('/:userId', deleteUser, responseHandler);

// Get all the users in
router.get('/', getAllUsers, responseHandler);

// Get user profile
router.get('/profile/:userId', getUserProfile, responseHandler);

router.post(
  '/upload/file',
  upload.single('file'),
  uploadTextFile,
  responseHandler
);

router.get('/download/file', downloadTextFile);

export default router;
