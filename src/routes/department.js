import { Router } from 'express';

import {
  addUserToDepartment,
  createDepartmentTable,
  getDepartmentCounts,
} from '../controllers/departmentController.js';
import { responseHandler } from '../utils/index.js';

const router = Router();

router.post('/table', createDepartmentTable);

router.post('/:userId', addUserToDepartment, responseHandler);

router.get('/count', getDepartmentCounts, responseHandler);

export default router;
