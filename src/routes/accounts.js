import { Router } from 'express';

import {
  createAccountTable,
  dropAccountTable,
} from '../controllers/accountController.js';

const router = Router();

router.post('/table', createAccountTable);

router.delete('/table', dropAccountTable);

export default router;
