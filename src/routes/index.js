import express from 'express';

import userRoutes from './users.js';
import accountRoutes from './accounts.js';
import departmentRoutes from './department.js';

const router = express.Router();

router.use('/users', userRoutes);
router.use('/accounts', accountRoutes);
router.use('/departments', departmentRoutes);

export default router;
