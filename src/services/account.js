import { query } from '../db/index.js';

export const createUserAccount = async user_id => {
  try {
    const text =
      'INSERT INTO accounts (user_id, balance) VALUES ($1, $2) RETURNING *';
    const params = [user_id, 0.0];
    const res = (await query(text, params)).rows[0];
    return res;
  } catch (err) {
    console.log(err);
    return { message: 'Something went wrong' };
  }
};
