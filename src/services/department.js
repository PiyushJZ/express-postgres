import { query } from '../db/index.js';

export const insertUserInDepartment = async (id, department) => {
  try {
    const text =
      'INSERT INTO department (user_id, department) VALUES ($1, $2) RETURNING *';
    const params = [id, department];
    const res = await query(text, params);
    return res.rows[0];
  } catch (err) {
    return { message: 'Could not add user to department' };
  }
};
