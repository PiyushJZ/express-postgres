import { query, client } from '../db/index.js';
import { countUserEntriesEvent } from '../utils/index.js';
import { createUserAccount } from './account.js';

export const createUser = async (firstName, lastName, gender, address) => {
  try {
    if (
      typeof firstName !== 'string' ||
      typeof lastName !== 'string' ||
      typeof gender !== 'boolean'
    ) {
      throw new TypeError();
    }
    const text =
      'INSERT INTO users (firstname, lastname, gender, ctime, address) VALUES ($1, $2, $3, current_timestamp, $4) RETURNING *';
    const values = [firstName, lastName, gender, address];
    const res = (await query(text, values)).rows[0];
    const account = await createUserAccount(res.id);
    countUserEntriesEvent();
    return account.message ? account : { createdUser: res, account };
  } catch (err) {
    console.log(err);
    return { message: 'Values are of wrong type' };
  }
};

export const fetchUser = async id => {
  try {
    const text =
      'SELECT id, firstname, lastname, gender, balance, currency FROM users INNER JOIN accounts ON id=user_id WHERE id=$1';
    const params = [id];
    const res = (await query(text, params)).rows[0];
    return res ? res : { message: 'User does not exist' };
  } catch (err) {
    return { message: 'Unable to fetch' };
  }
};

export const deleteUserRecord = async id => {
  const transactionClient = await client();
  try {
    // Starting transaction
    await transactionClient.query('BEGIN');
    console.log(id);
    const params = [id];

    // Deleting from  accounts
    const accountText = 'DELETE FROM accounts WHERE user_id=$1';
    const res = await transactionClient.query(accountText, params);
    // console.log(res);
    if (res.rowCount === 0) return { message: 'User not found' };
    // console.log(res.rowCount);
    // Deleting from users
    const userText = 'DELETE FROM users WHERE id=$1';
    await transactionClient.query(userText, params);

    // Completing the transaction
    await transactionClient.query('COMMIT');
    return { status: 'success' };
  } catch (err) {
    console.log(err);
    await transactionClient.query('ROLLBACK');
    return { message: 'Unable to delete user' };
  } finally {
    transactionClient.release();
  }
};

export const fetchAllUsers = async () => {
  const text = 'SELECT * FROM users ORDER BY id DESC';
  const res = await query(text);
  return res.rows;
};

export const fetchUserProfile = async id => {
  try {
    const text =
      "Select id, firstname, lastname, gender, ARRAY_AGG(department) AS departments, address['address']['street_addr'] as street_address, address['address']['zip_code'] as zip_code FROM users INNER JOIN department ON id=user_id WHERE id=(SELECT id FROM users WHERE id=$1) GROUP BY id";
    const params = [id];

    const res = await query(text, params);
    return res.rows;
  } catch (err) {
    return { message: 'Unable to fetch user profile' };
  }
};

export const getUserEntries = async () => {
  const text = 'SELECT COUNT(id) FROM users';
  const res = await query(text);
  return res.rows[0];
};
